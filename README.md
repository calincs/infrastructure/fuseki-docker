# Docker Container for Jena-Fuseki

This is an Apache Jena Fuseki Docker image inspired by the following sources:

* https://github.com/stain/jena-docker/tree/master/jena-fuseki
* https://github.com/blankdots/docker-SemanticWebApps/tree/master/apache-fuseki
* https://github.com/charlesvardeman/fuseki-geosparql-docker
* https://github.com/maximelefrancois86/fuseki-docker
* https://github.com/SemanticComputing/fuseki-docker

Empty LINCS datasets are configured by default with full-text indexes. There is also a Helm chart for deployment to Kubernetes.

## Run Fuseki locally

```bash
git clone https://gitlab.com/calincs/infrastructure/fuseki-docker
cd fuseki-docker
docker compose up
```

Open the admin UI at [http://localhost:3030](http://localhost:3030). Username/password is _admin/admin_.
