FROM eclipse-temurin:17.0.10_7-jre

ENV FUSEKI_VERSION 5.3.0
ENV FUSEKI_MIRROR https://downloads.apache.org
ENV FUSEKI_ARCHIVE http://archive.apache.org/dist

LABEL maintainer="zacanbot@gmail.com"
LABEL name="zacanbot/fuseki"
LABEL org.opencontainers.image.source https://gitlab.com/calincs/infrastructure/fuseki-docker
LABEL org.opencontainers.image.title "Apache Jena Fuseki"
LABEL org.opencontainers.image.description "Fuseki is a SPARQL 1.1 server with a web interface, backed by the Apache Jena TDB RDF triple store."
LABEL org.opencontainers.image.version ${FUSEKI_VERSION}
LABEL org.opencontainers.image.licenses "(Apache-2.0 AND (GPL-2.0 WITH Classpath-exception-2.0) AND GPL-3.0)"
LABEL org.opencontainers.image.authors "Apache Jena Fuseki by https://jena.apache.org/; this image by zacanbot"

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
      bash curl ca-certificates findutils coreutils pwgen procps wget nano less

# Installation folder
ENV FUSEKI_HOME /jena-fuseki
RUN mkdir ${FUSEKI_HOME}
# Config and data volume
ENV FUSEKI_BASE=/data/fuseki

WORKDIR /tmp
# Download/unpack/move in one go (to reduce image size)
RUN wget $FUSEKI_MIRROR/jena/binaries/apache-jena-fuseki-$FUSEKI_VERSION.tar.gz \
    || wget $FUSEKI_ARCHIVE/jena/binaries/apache-jena-fuseki-$FUSEKI_VERSION.tar.gz \
    && tar zxf apache-jena-fuseki-$FUSEKI_VERSION.tar.gz \
    && mv apache-jena-fuseki-$FUSEKI_VERSION/* $FUSEKI_HOME \
    && rm apache-jena-fuseki-$FUSEKI_VERSION.tar.gz \
    && cd $FUSEKI_HOME && rm -rf fuseki.war

ENV GOSU_VERSION 1.17
RUN apt-get install -y gosu; \
    gosu nobody true

# Adding user without root privileges alongside a user group
RUN groupadd fuseki \
    && useradd -g fuseki fuseki

# Enable basic-auth with a preset admin password
COPY shiro.ini $FUSEKI_HOME/shiro.ini
COPY docker-entrypoint.sh /

# Customising config. Will be copied over to FUSEKI_BASE by start-fuseki.sh
RUN mkdir -p $FUSEKI_HOME/config/datasets
COPY datasets/ $FUSEKI_HOME/config/datasets
COPY config.ttl $FUSEKI_HOME/config/config.ttl
RUN mkdir -p $FUSEKI_HOME/extra
COPY extra/* $FUSEKI_HOME/extra

COPY start-fuseki.sh $FUSEKI_HOME
RUN chmod 755 $FUSEKI_HOME/start-fuseki.sh $FUSEKI_HOME/fuseki-server /docker-entrypoint.sh

ENV GOSU_USER fuseki:fuseki
# setting directories to be owned by the non-root user
ENV GOSU_CHOWN $FUSEKI_HOME $FUSEKI_BASE

WORKDIR $FUSEKI_HOME
EXPOSE 3030
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD $FUSEKI_HOME/start-fuseki.sh