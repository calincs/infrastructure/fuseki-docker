#!/bin/bash

# set -e

# move extra jar files from the home folder to the mounted data folder
mkdir -p $FUSEKI_BASE/extra
cp -nr $FUSEKI_HOME/extra/* $FUSEKI_BASE/extra/

# move custom config to FUSEKI_BASE (don't overwrite)
mkdir -p $FUSEKI_BASE/config \
  && mkdir -p $FUSEKI_BASE/configuration
cp -nr $FUSEKI_HOME/config/datasets/*.ttl $FUSEKI_BASE/configuration/
cp -nr $FUSEKI_HOME/config/config.ttl $FUSEKI_BASE/config.ttl
rm -rf $FUSEKI_HOME/databases
ln -s $FUSEKI_BASE/databases $FUSEKI_HOME/databases

# If GOSU_CHOWN environment variable set, recursively chown all specified directories
# to match the user:group set in GOSU_USER environment variable.
if [ -n "$GOSU_CHOWN" ]; then
    for DIR in $GOSU_CHOWN
    do
        chown -R $GOSU_USER $DIR
    done
fi

FUSEKI_OPTS=${FUSEKI_OPTS:-"--config $FUSEKI_BASE/config.ttl"}
cd $FUSEKI_HOME
# If GOSU_USER environment variable set to something other than 0:0 (root:root),
# become user:group set within and exec command passed in args
if [ "$GOSU_USER" != "0:0" ]; then
#    exec gosu $GOSU_USER ./fuseki-server $FUSEKI_OPTS "$@"
    exec gosu $GOSU_USER java -jar fuseki-server.jar
fi

# If GOSU_USER was 0:0 exec command passed in args without gosu (assume already root)

exec "$@"